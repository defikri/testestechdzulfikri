<?php

namespace App\Http\Controllers;

use App\Person_data;
use Illuminate\Http\Request;
use DataTables;
use Redirect, Response, Validator;
Use Image;
use Intervention\Image\Exception\NotReadableException;
class DataPerson extends Controller
{
    public function index(Request $request){
    	return view('data');
    }

    public function tampil(Request $request){
      if(request()->ajax()){
        $data = Person_data::latest()->get();
        return DataTables::of($data)
        ->addIndexColumn()
        ->addColumn('action', function($row){
          $btn = '<a href="javascript:void(0)" data-toggle="tooltip" data-id="'.$row->person_id.'" data-original-title="Edit Data" title="Edit Data" class="btn btn-success btn-sm editPerson"><i class="fa fa-edit"></i></a>';
          $btn = $btn.'&nbsp;&nbsp;<a href="javascript:void(0)" data-toggle="tooltip" data-id="'.$row->person_id.'" data-original-title="Hapus Data" title="Hapus Data" class="btn btn-danger btn-sm deletePerson"><i class="fa fa-trash"></i></a>';
          return $btn;
        })
        ->rawColumns(['action'])
        ->make(true);
      }
    }
    public function simpan (Request $request){
    	$validator = Validator::make($request->all(), [
    		"person_name"				=> 'required|regex:/^[a-zA-Z ]+$/u',
    		"person_birth"			=> 'required|date:Y-m-d',
    		"person_address"		=> 'required|regex:/^[a-zA-Z0-9\s]+/u',
    		"person_email"			=> 'required|email',
    		"person_filephoto"	=> 'required|mimes:jpg,jpeg|max:20000'
    	],
    	[
    		"person_name.required" 			=> "Field Nama Kosong",
    		"person_birth.required" 		=> "Field Tanggal Lahir Kosong",
    		"person_address.required" 	=> "Field Alamat Kosong",
    		"person_email.required" 		=> "Field Email Kosong",
    		"person_filephoto.required" => "File Foto Kosong",
    		"person_name.regex"					=> "Field Nama Hanya Boleh Diisi dengan Alfabet dan Spasi",
    		"person_birth.date"					=> "Format Tanggal Harus Tahun-Bulan-Tanggal, Misalkan 2019-01-11",
    		"person_address.regex"	=> "Format Alamat Tidak sesuai",
    		"person_email.email"				=> "Format Email Salah",
    		"person_filephoto.mimes"		=> "Format Ekstensi Gambar yang diizinkan Hanya JPG dan JPEG",
    		"person_filephoto.max"			=> "Tidak boleh lebih dari 20 MB"			
    	]
    	);

    	if($validator->passes()){
  			$image = $request->file('person_filephoto');
  			$originalfile = $image->getClientOriginalName();
  			$originalname = pathinfo($originalfile, PATHINFO_FILENAME);
  			$destination_path	= public_path('/uploads/');
    		if(file_exists($destination_path.$originalname.'-600x600.jpg')||file_exists($destination_path.$originalname.'-240x240.jpg')){
    			$originalname.= str_replace('-', '', date('Y-m-d')).str_replace(':', '', date('h:i:s'));
    		}
    		$filetostore 	= $originalname.'-original.jpg';
    		$imagename600	= $originalname.'-600x600.jpg';
    		$imagename240 = $originalname.'-240x240.jpg';
    		$image->move($destination_path, $filetostore);
    		$resizing = public_path('/uploads/'.$filetostore);
    		$img600 = Image::make($resizing)->fit(600,600, function($constraint){
    			$constraint->upsize();
    		})->save($destination_path.$imagename600);
    		$img240 = Image::make($resizing)->fit(240,240, function($constraint){
    			$constraint->upsize();
    		})->save($destination_path.$imagename240);
    		$post = Person_data::insert(['person_name'=>$request->person_name, 'person_birth'=>$request->person_birth, 'person_address'=>$request->person_address, 'person_email'=>$request->person_email, 'person_filephoto'=>$imagename600.':'.$imagename240]);
    		unlink($destination_path.$filetostore);
  			return response()->json(['success'=>'Berhasil Menyimpan Data']);
    		
    	}else{
    		return response()->json(['error'=>$validator->errors()->all()]);
    	}
    }

    public function updateData(Request $request){
    	$validator = Validator::make($request->all(), [
    		"edit_person_name"			=> 'required|regex:/^[a-zA-Z ]+$/u',
    		"edit_person_birth"			=> 'required|date:Y-m-d',
    		"edit_person_address"		=> 'required|regex:/^[a-zA-Z0-9\s]+/u',
    		"edit_person_email"			=> 'required|email',
    		"edit_person_filephoto"	=> 'required|mimes:jpg,jpeg|max:20000'
    	],
    	[
    		"edit_person_name.required" 		 => "Field Nama Kosong",
    		"edit_person_birth.required" 		 => "Field Tanggal Lahir Kosong",
    		"edit_person_address.required" 	 => "Field Alamat Kosong",
    		"edit_person_email.required" 		 => "Field Email Kosong",
    		"edit_person_filephoto.required" => "File Foto Kosong",
    		"edit_person_name.regex"				 => "Field Nama Hanya Boleh Diisi dengan Alfabet dan Spasi",
    		"edit_person_birth.date"				 => "Format Tanggal Harus Tahun-Bulan-Tanggal, Misalkan 2019-01-11",
    		"edit_person_address.regex"  	 => "Format Alamat Tidak sesuai",
    		"edit_person_email.email"				 => "Format Email Salah",
    		"edit_person_filephoto.mimes"		 => "Format Ekstensi Gambar yang diizinkan Hanya JPG dan JPEG",
    		"edit_person_filephoto.max"			 => "Tidak boleh lebih dari 20 MB"			
    	]
    	);

    	if($validator->passes()){
    		$get_personphoto = Person_data::where('person_id',$request->edit_person_id)->first();
    		$explode_photo  = explode(':', $get_personphoto->person_filephoto);
    		if(file_exists(public_path('/uploads/'.$explode_photo[0]))||file_exists(public_path('/uploads/'.$explode_photo[1]))){
          unlink(public_path('/uploads/'.$explode_photo[0])); 
          unlink(public_path('/uploads/'.$explode_photo[1]));
        }
  			$image = $request->file('edit_person_filephoto');
  			$originalfile = $image->getClientOriginalName();
  			$originalname = pathinfo($originalfile, PATHINFO_FILENAME);
  			$destination_path	= public_path('/uploads/');
    		if(file_exists($destination_path.$originalname.'-600x600.jpg')||file_exists($destination_path.$originalname.'-240x240.jpg')){
    			$originalname.= str_replace('-', '', date('Y-m-d')).str_replace(':', '', date('h:i:s'));
    		}
    		$filetostore 	= $originalname.'-original.jpg';
    		$imagename600	= $originalname.'-600x600.jpg';
    		$imagename240 = $originalname.'-240x240.jpg';
    		$image->move($destination_path, $filetostore);
    		$resizing = public_path('/uploads/'.$filetostore);
    		$img600 = Image::make($resizing)->fit(600,600, function($constraint){
    			$constraint->upsize();
    		})->save($destination_path.$imagename600);
    		$img240 = Image::make($resizing)->fit(240,240, function($constraint){
    			$constraint->upsize();
    		})->save($destination_path.$imagename240);
    		Person_data::where('person_id', $request->edit_person_id)->update(['person_name'=>$request->edit_person_name, 'person_birth'=>$request->edit_person_birth, 'person_address'=>$request->edit_person_address, 'person_email'=>$request->edit_person_email, 'person_filephoto'=>$imagename600.':'.$imagename240]);
    		unlink($destination_path.$filetostore);
  			return response()->json(['success'=>'Berhasil Mengubah Data']);
    		
    	}else{
    		return response()->json(['error'=>$validator->errors()->all()]);
    	}
    	
    	return Response::json($post);
    }

    public function edit($id){
    	$where 	= array('person_id'=>$id);
    	$post 	= Person_data::where($where)->first();
    	return Response::json($post);
    }

    public function hapus($id){
    	$get_personphoto = Person_data::where('person_id',$id)->first();
    	$explode_photo  = explode(':', $get_personphoto->person_filephoto);
    	if(file_exists(public_path('/uploads/'.$explode_photo[0]))||file_exists(public_path('/uploads/'.$explode_photo[1]))){
        unlink(public_path('/uploads/'.$explode_photo[0])); 
        unlink(public_path('/uploads/'.$explode_photo[1]));
      }
    	Person_data::where('person_id', $id)->delete();
    	return response()->json(['success'=>'Berhasil Menghapus Data']);
    }

    
}
