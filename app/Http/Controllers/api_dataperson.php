<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Person_data;
use App\Http\Resources\api_persondata as PersonResources;
use App\Http\Resources\api_persondataCollection;
class api_dataperson extends Controller
{
    public function index(){
    	return new api_persondataCollection(Person_data::get());
    }
}
