<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class api_persondata extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        $explodephoto = explode(':', $this->person_filephoto);
        return [ 
            'nama'      => $this->person_name,
            'alamat'    => $this->person_address,
            'tgl_lahir' => $this->person_birth,
            'email'     => $this->person_email,
            'image' =>[
                "600x600" => $explodephoto[0],
                "240x240" => $explodephoto[1],
            ]
        ];
    }
}
