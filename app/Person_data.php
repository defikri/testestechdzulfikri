<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person_data extends Model
{
    //
	protected $fillable = [
		'person_name', 'person_birth', 'person_address', 'person_email', 'person_filephoto'
	];
}
