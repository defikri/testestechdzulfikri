<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person_datas', function (Blueprint $table) {
            $table->increments('person_id');
            $table->string('person_name');
            $table->date('person_birth');
            $table->text('person_address');
            $table->string('person_email');
            $table->text('person_filephoto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('person_datas');
    }
}
