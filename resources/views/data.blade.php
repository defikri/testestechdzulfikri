<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Test | Biodata</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/bower_components/Ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue-light layout-top-nav">
  <div class="wrapper">
    <div class="content-wrapper">
      <div class="container">
        <section class="content">
          <h2 align="center">Biodata Personal</h2>
          <br/>
          <div class="row">
            <div class="col-xs-2"></div>
            <div class="col-xs-8">
              <div id="notif_simpan_biodata"></div>
              <form id="simpan_biodata_diri" dataparsley-calidate class="form-horizontal form-label-left">
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-xs-6">
                      <label class="control-label">
                        Nama
                        <span class="required"></span>
                      </label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                        </div>
                        <input type="text" class="form-control pull-right"  autocomplete="off" name="person_name">
                      </div>
                    </div>
                    <div class="col-xs-6">
                      <label class="control-label">
                        Tanggal Lahir
                        <span class="required">*</span>
                      </label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control tanggal_picker" autocomplete="off" name="person_birth">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label">
                    Alamat
                    <span class="required">*</span>
                  </label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-home"></i>
                    </div>
                    <textarea class="form-control pull-right" name="person_address"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-xs-6">
                      <label class="control-label">
                        Email
                        <span class="required">*</span>
                      </label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-envelope-o"></i>
                        </div>
                        <input type="email" class="form-control pull-right" autocomplete="off" name="person_email">
                      </div>
                    </div>
                    <div class="col-xs-6">
                      <label class="control-label">
                        Foto
                        <span class="required">*</span>
                      </label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-file-image-o"></i>
                        </div>
                        <input type="file" class="form-control pull-right" autocomplete="off" name="person_filephoto">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group" style="margin-top: 20px">
                  <div class="pull-right">
                    <button class="btn btn-danger" type="button" onclick="reset_form()"><span><i class="fa fa-close"></i></span>&nbsp;&nbsp;Cancel</button>
                    <button type="submit" class="btn btn-success" id="btn_simpan_data"><span><i class="fa fa-save"></i></span>&nbsp;&nbsp;Simpan Data</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="col-xs-2"></div>
          </div>
          <br/>
          <div class="row" style="margin-top: 10px;">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"><strong><i class="fa fa-table"></i> Tabel Person Data</strong></h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool"  data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <div class="box-body" style="overflow-x: auto; padding: 15px;">
                  <table id="tabel_persondata" class="table table-bordered table-hover">
                    <thead>
                      <th>ID</th>
                      <th>Nama</th>
                      <th>Tanggal Lahir</th>
                      <th>Alamat</th>
                      <th>Email</th>
                      <th>Foto</th>
                      <th style="width:20%">Action</th>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
            <div class="col-xs-1"></div>
          </div>
        </section>
      </div>
      {{ view('modal_update')}}
    </div>
  </div>
  <script src="{{ asset('assets/bower_components/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
  <script src="{{ asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('assets/bower_components/moment/min/moment.min.js')}} "></script>
  <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
  <script src="{{ asset('assets/bower_components/fastclick/lib/fastclick.js') }}"></script>
  <script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>
  <script src="{{ asset('assets/dist/js/demo.js') }}"></script>
  <script src="{{ asset('assets/bower_components/jquery-validate/jquery.validate.min.js')}}"></script>

  <script type="text/javascript">

    $('.tanggal_picker').datepicker({
      format: 'yyyy-mm-dd',
      autoclose : true
    });

    $(document).ready(function(){

      $.ajaxSetup({
        headers:{
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $('#tabel_persondata').DataTable({
        processing  : true,
        serverSide  : true,
        ajax        : {
          url   : "{{ URL::to('persondata/tampil')}}",
          type  : "GET"
        },
        columns : [
          { data : 'person_id', name : 'person_id', 'visible':false},
          { data : 'person_name', name : 'person_name'},
          { data : 'person_birth', name : 'person_birth'},
          { data : 'person_address', name : 'person_address'},
          { data : 'person_email', name : 'person_email'},
          { data : 'person_filephoto', name : 'person_filephoto'},
          { data : 'action', name: 'action', orderable:false}
        ],
        order: [[0,'desc']]
      });

      $('body').on('click', '.editPerson', function(){
        var person_id = $(this).data('id');
        $.get('persondata/edit/'+person_id, function (data){
          reset_form('#update_biodata_diri');
          $('[name="edit_person_id"]').val(person_id);
          $('[name="edit_person_name"]').val(data.person_name);
          $('[name="edit_person_birth"]').datepicker('setDate', data.person_birth);
          $('[name="edit_person_address"]').val(data.person_address);
          $('[name="edit_person_email"]').val(data.person_email);
          $('#modal_update_person').modal('show');
        })
      });

      $('body').on('click', '.deletePerson', function(){
        var person_id = $(this).data('id');
        if(confirm("Are you sure want to delete this data ? ")){
          $.ajax({
            type  : "GET",
            url   : "persondata/hapus/"+person_id,
            success : function(data){
              notif_sukses(data.success, '#notif_simpan_biodata');
              refresh_table();
            },
            error : function(data){
              console.log('Error : ', data);
            }
          })
        }
      });

      $('#simpan_biodata_diri').submit(function(e){
        e.preventDefault();
        var actionType = $('#btn_simpan_data').val();
        $('#btn_simpan_data').html('<span><i class="fa fa-refresh fa-spin"></i></span>&nbsp;&nbsp;Sending...');
        $.ajax({
          data        : new FormData(this),
          url         : "{{ URL::to('persondata/simpan') }}",
          type        : "POST",
          dataType    : "JSON",
          contentType : false,
          catch       : false,
          processData : false,
          beforeSend: function(xhr){
            xhr.setRequestHeader('X-CSRF-TOKEN', $('[name="csrf-token"]').attr('content'));
          },
          success     : function(data) {
            if($.isEmptyObject(data.error)){
              reset_form('#simpan_biodata_diri');
              $('#btn_simpan_data').html('<span><i class="fa fa-save"></i></span>&nbsp;&nbsp;Simpan Data');
              refresh_table();
              notif_sukses('Berhasil Menyimpan Data', '#notif_simpan_biodata');
            }else{
              print_error_message(data.error, '#notif_simpan_biodata');
              $('#btn_simpan_data').html('<span><i class="fa fa-save"></i></span>&nbsp;&nbsp;Simpan Data');
            }
          },
          error    : function(data){
            console.log('Error : ', data);
            $('#btn_simpan_data').html('<span><i class="fa fa-save"></i></span>&nbsp;&nbsp;Simpan Data');
          }
        });
      });

      $('#update_biodata_diri').submit(function(e){
        e.preventDefault();
        var actionType = $('#btn_update_data').val();
        $('#btn_update_data').html('<span><i class="fa fa-refresh fa-spin"></i></span>&nbsp;&nbsp;Sending...');
        $.ajax({
          data        : new FormData(this),
          url         : "{{ URL::to('persondata/update') }}",
          type        : "POST",
          dataType    : "JSON",
          contentType : false,
          catch       : false,
          processData : false,
          beforeSend: function(xhr){
            xhr.setRequestHeader('X-CSRF-TOKEN', $('[name="csrf-token"]').attr('content'));
          },
          success     : function(data) {
            if($.isEmptyObject(data.error)){
              reset_form('#update_biodata_diri');
              $('#btn_update_data').html('<span><i class="fa fa-save"></i></span>&nbsp;&nbsp;Update Data');
              $('#modal_update_person').modal('hide');
              refresh_table();
              notif_sukses(data.success, '#notif_simpan_biodata');

            }else{
              print_error_message(data.error, '#notif_update_data');
              $('#btn_update_data').html('<span><i class="fa fa-save"></i></span>&nbsp;&nbsp;Update Data');
            }
          },
          error    : function(data){
            console.log('Error : ', data);
            $('#btn_update_data').html('<span><i class="fa fa-save"></i></span>&nbsp;&nbsp;Update Data');
          }
        });
      });
    });

    function refresh_table(){
      $('#tabel_persondata').DataTable().ajax.reload();
    }

    function reset_form(id_form){
      $(id_form)[0].reset();
    }

    function notif_sukses(keterangan, id_notif){
      $(id_notif).html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Sukses!</h4>'+keterangan+'</div>');
    }

    function print_error_message(keterangan, id_notif){
      $(id_notif).html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-close"></i> Error Menyimpan Data!</h4>Error dalam menyimpan data Karena : <p></p></div>');
      var incr = 0;
      $.each(keterangan, function(key, value){
        if(incr == 0){
          $(id_notif).find('p').append(value);
        }else{
          $(id_notif).find('p').append(' &nbsp; ,  &nbsp; '+value);
        }
        incr++;
      });
    }
  </script>
</body>
</html>
