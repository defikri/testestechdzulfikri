<div class="modal fade" id="modal_update_person" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-green">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Person Data</h4>
      </div>
      <div id="notif_update_data"></div>
      <form id="update_biodata_diri" dataparsley-calidate class="form-horizontal form-label-left">
        <div class="modal-body modal-tab-container">
          <div class="clearfix">
            <div class="col-md-12 col-xs-12">
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-6">
                    <label class="control-label">
                      Nama
                      <span class="required"></span>
                    </label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-user"></i>
                      </div>
                      <input type="text" class="form-control pull-right"  autocomplete="off" name="edit_person_name">
                    </div>
                  </div>
                  <div class="col-xs-6">
                    <label class="control-label">
                      Tanggal Lahir
                      <span class="required">*</span>
                    </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control tanggal_picker" autocomplete="off" name="edit_person_birth">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label">
                  Alamat
                  <span class="required">*</span>
                </label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-home"></i>
                  </div>
                  <textarea class="form-control pull-right" name="edit_person_address"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-6">
                    <label class="control-label">
                      Email
                      <span class="required">*</span>
                    </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-envelope-o"></i>
                      </div>
                      <input type="email" class="form-control pull-right" autocomplete="off" name="edit_person_email">
                    </div>
                  </div>
                  <div class="col-xs-6">
                    <label class="control-label">
                      Foto
                      <span class="required">*</span>
                    </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-file-image-o"></i>
                      </div>
                      <input type="file" class="form-control pull-right" autocomplete="off" name="edit_person_filephoto">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" name="edit_person_id">
        <div class="modal-footer bg-green">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-outline" id="btn_update_data"><span><i class="fa fa-edit"></i></span>&nbsp;&nbsp;  Update Data</button>
        </div>
      </form>
      
    </div>
    <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>