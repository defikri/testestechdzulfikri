<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('welcome');
});

Route::get('persondata', 'DataPerson@index');
Route::get('persondata/tampil', 'DataPerson@tampil');
Route::post('persondata/simpan', 'DataPerson@simpan');
Route::post('persondata/update', 'DataPerson@updateData');
Route::get('persondata/edit/{id}', 'DataPerson@edit');
Route::get('persondata/hapus/{id}', 'DataPerson@hapus');
